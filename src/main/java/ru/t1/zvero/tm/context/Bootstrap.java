package ru.t1.zvero.tm.context;

import ru.t1.zvero.tm.api.ICommandController;
import ru.t1.zvero.tm.api.ICommandRepository;
import ru.t1.zvero.tm.api.ICommandService;
import ru.t1.zvero.tm.constant.ArgumentConst;
import ru.t1.zvero.tm.constant.CommandConst;
import ru.t1.zvero.tm.controller.CommandController;
import ru.t1.zvero.tm.repository.CommandRepository;
import ru.t1.zvero.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(String[] args) {
        processArguments(args);
        processCommand();
    }

    private void processCommand() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArguments(argument);
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.EXIT:
                commandController.showExit();
                break;
            default:
                commandController.showErrorCommand();
                break;
        }
    }

    private void processArguments(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument();
                break;
        }
        System.exit(0);
    }

}