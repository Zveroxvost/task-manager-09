package ru.t1.zvero.tm;

import ru.t1.zvero.tm.context.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}